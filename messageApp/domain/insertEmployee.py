#!/usr/bin/python

import MySQLdb

# Open database connection
db = MySQLdb.connect("localhost","messagedbowner","messagedbowner","messagedb" )

# prepare a cursor object using cursor() method
cursor = db.cursor()

# Prepare SQL query to INSERT a record into the database.
sql = """INSERT INTO STUDENT
         VALUES (1,'YOGESH')"""
try:
   # Execute the SQL command
   cursor.execute(sql)
   # Commit your changes in the database
   db.commit()
except:
   # Rollback in case there is any error
   db.rollback()

# disconnect from server
db.close()
