#!/usr/bin/python

import MySQLdb

db = MySQLdb.connect("localhost","messagedbowner","messagedbowner","messagedb" )

cursor = db.cursor()

cursor.execute("DROP TABLE IF EXISTS STUDENT")

sql = """CREATE TABLE STUDENT (
         STUDENT_ID INT(3) NOT NULL,
	 STUDENT_NAME VARCHAR(64),
	 PRIMARY KEY(STUDENT_ID) )"""

cursor.execute(sql)

db.close()
