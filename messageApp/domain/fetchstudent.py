import MySQLdb

# Open database connection
db = MySQLdb.connect("localhost","messagedbowner","messagedbowner","messagedb" )

# prepare a cursor object using cursor() method
cursor = db.cursor()

sql = "SELECT * FROM STUDENT WHERE STUDENT_ID = '%d'" % (1)
try:
   # Execute the SQL command
   cursor.execute(sql)
   # Fetch all the rows in a list of lists.
   results = cursor.fetchall()
   for row in results:
      STUDENT_ID = row[0]
      STUDENT_NAME = row[1]
      # Now print fetched result
      print ("STUDENT_ID=%s,STUDENT_NAME=%s" % (STUDENT_ID, STUDENT_NAME))
except:
   print "Error: unable to fecth data"

# disconnect from server
db.close()
