class Message:

#	def __init__(self,messageId,messageString):
#		self.__messageId = messageId
#		self.__messageString = messageString
		
	@property
	def messageId(self):
                return self.__messageId
        
        @property
        def messageString(self):
                return self.__messageString
        
        @messageId.setter
        def messageId(self,messageId):
                self.__messageId = messageId

        @messageString.setter
        def messageString(self,messageString):
                self.__messageString = messageString
                
	def __repr__(self):
		return 'Message(messageId=%s, messageString=%s)' % (self.messageId, self.messageString)

#message1 = Message()
#message1.messageId=1
#message1.messageString='One'
#print(message1)
#message2 = Message(2,"Two")
#print(message2)
#messageId=int(input("enter message id"))
#messageString=raw_input("enter message string")
#message = Message()
#message.messageId=messageId
#message.messageString=messageString
#print(message)

