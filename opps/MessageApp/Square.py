class Square(object):
	def __init__(self,length):
		self.length = length
	@property
	def length(self):
		return self.__length
	@length.setter
	def length(self,value):
		self.__length = value
	@length.deleter
	def length(self):
		del self.__length
r = Square(5)
print(r.length)
r.length=9
print(r.length)
