import abc
class Bird(abc.ABCMeta):
	def __init__(self,bird_name):
		self.bird =bird_name
	@abc.abstractmethod
	def fly(self):
		pass

class Parrot(Bird):
	def fly(self):
		print("Parrot flying")

p = Parrot("birdd")
p.fly()
