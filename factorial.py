def calc_factorial(x):
    """This is a recursive function
    to find the factorial of an integer"""

    if x == 1:
        return 1
    else:
        return (x * calc_factorial(x-1))

x=input('enter x')
intX=int(x)
print("The factorial of", intX, "is", calc_factorial(intX))	
