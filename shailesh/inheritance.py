class Animal:
    legs = 2

    def __init__(self):
        self.legs = 2
    
    def __init__(self, legs):
       self.legs = legs;
       
    def eat(self):
        print("All animals eat");

class Dog(Animal):
    def eat(self):
        print("Dog eat meat")
        
    def bark(self):
        print("Dog can bark");

    def showDogInfo(self):
        print(self.legs);

class Chicken(Animal):
    def showChickenInfo(self):
        print(self.legs);    

d = Dog(4)
d.eat();
d.bark();
d.showDogInfo();

c = Chicken(2);
c.showChickenInfo();


