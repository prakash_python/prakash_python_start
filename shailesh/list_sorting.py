'''
Ref Link: 
https://wiki.python.org/moin/HowTo/Sorting#Sortingbykeys
https://www.programiz.com/python-programming/methods/built-in/sorted

'''


from operator import itemgetter, attrgetter, methodcaller

class Student:
    'documentation'
    def __init__(self, name='abc', age=1):
        self.name = name
        self.age = age

    def __repr__(self):
        return repr((self.name, self.age))

s1 = Student('Shailesh', 29)
s2 = Student("Prakash", 40)
print(s1)
print(s2)

student_list = [s1, s2, Student('Amit', 25)]

print(student_list)

#student_list.sort();

sorted_student_list = sorted(student_list, key=attrgetter('name'), reverse=False)


print(sorted_student_list)

'''
fruit_list = ["Banana", "Apple", "Orange"];

print(sorted(fruit_list))

print(fruit_list)

# vowels list
pyList = ['e', 'a', 'u', 'o', 'i']
print(sorted(pyList))

# string 
pyString = 'Python'
print(sorted(pyString))

# vowels tuple
pyTuple = ('e', 'a', 'u', 'o', 'i')
print(sorted(pyTuple))
'''