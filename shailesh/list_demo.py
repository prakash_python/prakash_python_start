fruit_list = ["Apple", "Banana", "Orange"] * 3

print(fruit_list[1:2])

table_5 = [5] * 10

print(table_5)




greet_list = ["Hello"] * 3

greet_list[2] = "Bye"

print(greet_list)

L = ('spam', 'spam', 'SPAM!')

#L[1] = "Trying modify"

print(L[1:3])

student_directory = {'Name': 'Shailesh', 'Age': 29, 'Gender': 'Male'}

print(student_directory)

#del student_directory['Age']
print("After delete")
print(student_directory)

#student_directory.clear();
#del student_directory

print(student_directory)

key_tuple = ('name', 'age')
new_directory = {}
#new_directory = new_directory.fromkeys(key_tuple);

new_directory = new_directory.fromkeys(key_tuple, 10)

print(new_directory)


#shuffling list

songs_list = [1, 2, 3, 4, 5]

print(songs_list)

print("after shuffling")

import random

random.shuffle(songs_list)

print(songs_list)

songs_list.sort()

print("after sort")

print(songs_list)

songs_list.reverse();

print("after reverse")

print(songs_list)