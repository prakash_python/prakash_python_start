# Import classes from your brand new package
from Animals import Mammals
from Animals import Birds
from Animals import Fishes
# Create an object of Mammals class & call a method of it
myMammal = Mammals()
myMammal.printMembers()
 
# Create an object of Birds class & call a method of it
myBird = Birds()
myBird.printMembers()

myFish = Fishes()
myFish.printMembers()
