from numpy import linspace, sin, pi, exp
import matplotlib.pyplot as mp
x=linspace(0, 10*pi, 500)
y=sin(x) * exp(-x/10)
mp.plot(x,y)
mp.show()
