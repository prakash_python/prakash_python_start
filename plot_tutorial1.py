import numpy as np
import matplotlib.pyplot as plt

#X = np.linspace(-np.pi, np.pi, 256, endpoint=True)
#C, S = np.cos(X), np.sin(X)

#plt.plot(X, C)
#plt.plot(X, S)

#plt.show();

#other program

n = 256
x = np.linspace(-np.pi,np.pi,n,endpoint=True)
y = np.sin(2*x)
plt.plot(x,y+1,color='blue',alpha=1.00)
plt.plot(x,y-1,color='blue',alpha=1.00)
plt.show()

